export let lookAt = {
	x:0,
	y:10,
	z:0,
}

export let position = {
	x:40,
	y:10,
	z:40,
}

export let up = {
	x:0,
	y:1,
	z:0,
}

export let cameraBoundingRadius = 3;