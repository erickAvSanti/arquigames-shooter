/*
export blender Z forward, Y up
*/
export function objParser(str,callback){
	let arr = str.split('\n')
	let materials = []
	let currentObject = {}
	let objects = []
	let material
	let info = {}
	let idx,idy,idz,vertex,normal,normals,vertices,faces,face,f,json
	let first_operation = true
	for(idx in arr){
		let line = arr[idx]
		if(line.startsWith('#')){
			continue
		}
		if(line.startsWith('mtllib ')){
			materials.push(line.replace('mtllib ',''))
		}
		if(line.startsWith('o ')){
			if(!first_operation){
				objects.push(currentObject)
				first_operation = false
			}
			currentObject = {}
			currentObject.name = line.replace('o ','')
		}
		if(line.startsWith('v ')){
			if(!currentObject.vertices)currentObject.vertices = []
			vertices = line.replace('v ','').split(' ')
			for(idy in vertices){
				vertex = vertices[idy]
				try{vertex = window.parseFloat(vertex)}catch(e){vertex = 0}
				vertices[idy] = vertex
			}
			currentObject.vertices.push(vertices)
		}
		if(line.startsWith('vn ')){
			if(!currentObject.normals)currentObject.normals = []
			normals = line.replace('vn ','').split(' ')
			for(idy in normals){
				normal = normals[idy]
				try{normal = window.parseFloat(normal)}catch(e){normal = 0}
				normals[idy] = normal
			}
			currentObject.normals.push(normals)
		}
		if(line.startsWith('usemtl ')){
			if(!currentObject.materials)currentObject.materials = []
			material = line.replace('usemtl ','') 
			if(material!='' && material!='None')currentObject.materials.push(material)
		}
		if(line.startsWith('f ')){
			if(!currentObject.faces)currentObject.faces = []
			faces = line.replace('f ','').split(' ')
			for(idy in faces){
				face = faces[idy]
				face = face.split('//')
				try{face[0] = window.parseInt(face[0])}catch(e){}
				try{face[1] = window.parseInt(face[1])}catch(e){}
				faces[idy] = {vertex:(face[0]-1),normal:(face[1]-1)}
			}
			currentObject.faces.push(faces)
		}
	}
	objects.push(currentObject)
	info.objects = objects
	info.materials = materials
	console.log(arr)
	console.log(info)
	if(typeof callback == "function" )callback(info)
}