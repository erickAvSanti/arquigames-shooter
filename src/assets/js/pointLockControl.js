
const jQuery = require('jquery');
let func = (function(){

	let instance = null;
	let element = null;
	let capp = null;
	let evenst_lock_installed = false;
	const dev_mode = process.env.NODE_ENV === 'development';
	class PointLockControl{
		constructor(_capp){
			capp = _capp;
			element = capp.getElement();
			jQuery(element).click(function(evt){
				if(instance){
					instance.elementClick();
				}
			});
			evenst_lock_installed = false;
		}
		start(){
		  if(!element.getContext){
		    return;
		  }
		  if(this.checkPropertyLock()){
		 		this.installEventsLock();
		    this.requestPointerLock();
		  }
		}
		checkPropertyLock(){
			return 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
		}
		requestPointerLock(){
			element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
			element.requestPointerLock();
		}
		havePointerLock(){
			return document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element;
		}
		installEventsLock(){
			if(evenst_lock_installed)return;
			evenst_lock_installed = true;
			document.addEventListener('pointerlockchange', instance.pointerLockChange, false);
      document.addEventListener('mozpointerlockchange', instance.pointerLockChange, false);
      document.addEventListener('webkitpointerlockchange', instance.pointerLockChange, false);

			document.addEventListener('pointerlockerror', instance.pointerLockError, false);
			document.addEventListener('mozpointerlockerror', instance.pointerLockError, false);
			document.addEventListener('webkitpointerlockerror', instance.pointerLockError, false);
		}
		pointerLockChange(){
			if (instance.havePointerLock()){
	      capp.hasLocked = true;
	      document.addEventListener("mousemove", instance.moveCallback, false);
		  }else{
		    capp.hasLocked = false;
		    document.removeEventListener('pointerlockchange', instance.changeCallback, false);
		    document.removeEventListener('mozpointerlockchange', instance.changeCallback, false);
		    document.removeEventListener('webkitpointerlockchange', instance.changeCallback, false);

				document.removeEventListener('pointerlockerror', instance.pointerLockError, false);
				document.removeEventListener('mozpointerlockerror', instance.pointerLockError, false);
				document.removeEventListener('webkitpointerlockerror', instance.pointerLockError, false);

		    document.removeEventListener("mousemove", instance.moveCallback, false);
		  }
		}
		pointerLockError() {
  		console.log("Error while locking pointer.");
		}
		moveCallback(e){
			let movementX = e.movementX || e.mozMovementX || e.webkitMovementX || 0;
	    let movementY = e.movementY || e.mozMovementY || e.webkitMovementY || 0;
			capp.rotateView(movementX, movementY); 
		}
		elementClick(){
			instance.start();
		}
	}

	return {
		getInstance(obj){
			if(!instance){
				instance = new PointLockControl(obj);
			}
			return instance;
		}
	};
})();

export default func;