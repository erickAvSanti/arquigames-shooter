const jQuery = require('jquery');
const THREE = require('three');
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { OBJLoader2 } from 'three/examples/jsm/loaders/OBJLoader2';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';
import CameraUtils from "@/assets/js/cameraUtils.js";
const glm = require('gl-matrix');

const _url_files = process.env.VUE_APP_URL_FILES;
const _url_files_blender = process.env.VUE_APP_URL_FILES + '/blender/';

import {objParser} from "@/assets/js/obj-parser.js";
const axios = require('axios');

const func = (function(){

  let instance  = null;
  let capp      = null;

  let scene = null;
  let camera = null;
  let gridHelper = null;

  let objects = [];
  let mouse2D = new THREE.Vector2();
  let raycaster = new THREE.Raycaster();
  let isShiftDown = false;
  let isCtrlDown = false;
  let theta = 45 * 0.5;
  let plane = null;
  let intersector = null;
  let step = 10;
  let half_step = 5;
  let step_total = 10;
  let tmpVec = new THREE.Vector3();
  let normalMatrix = new THREE.Matrix3();

  let cubeGeo = null;
  let cubeMaterial = null;

  let pendingJson = null;

  let isMouseDown = false;

  let sphereBullet = new THREE.SphereBufferGeometry( 2, 16, 8 );


  let lightBullet = new THREE.PointLight( 0xEFF792, 1 , 50 );
  let bulletMaterial = new THREE.MeshBasicMaterial( { color : 0xEFF792 , transparent : true, opacity : 0.5 } );
  let intervalBulletCountDef = 15;
  let intervalBulletCount = intervalBulletCountDef;
  let lightBulletsInstances = [];

  const dev_mode = process.env.NODE_ENV === 'development'
  let cameraUtils;

  let collisionables = []

  let tmp_vector3 = new THREE.Vector3()

  class World{
    constructor(_capp){
      capp = _capp;
      cameraUtils = _capp.getCameraUtils();
    }
    init(){
      scene = capp.getScene();
      camera = capp.getCamera();
      gridHelper = new THREE.GridHelper( step_total*step, step_total );
      scene.add( gridHelper );

      var geometry = new THREE.PlaneBufferGeometry( step_total*step, step_total*step );
      geometry.rotateX( - Math.PI / 2 );
      plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );
      scene.add( plane );
      objects.push(plane);

      jQuery(document).mousemove(function(evt){
        instance.onDocumentMouseMove(evt);
      });
      jQuery(document).mousedown(function(evt){
        instance.onDocumentMouseDown(evt);
      });
      jQuery(document).mouseup(function(evt){
        instance.onDocumentMouseUp(evt);
      });
      jQuery(document).keydown(function(evt){
        instance.onDocumentKeyDown(evt);
      });
      jQuery(document).keyup(function(evt){
        instance.onDocumentKeyUp(evt);
      });

      let loader = new THREE.TextureLoader();
      let _this = this;

      loader.load( 
        require('@/assets/images/texture.png'), 
        function ( texture ) { 
          cubeGeo = new THREE.BoxGeometry( step, step, step );
          cubeMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c, flatShading: true, map: texture,transparent:true,opacity:1 } ); 
          if(dev_mode){
            console.log("texture",texture);
            console.log("cubeMaterial",cubeMaterial);
          }
          if( pendingJson ){
            //instance.addVoxelFromJson( pendingJson );
          }
        }, 
        function ( xhr ) {
          if(dev_mode)console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
        }, 
        function ( xhr ) {
          if(dev_mode)console.log( 'An error happened' );
        }
      );  

      var mtlLoader = new MTLLoader();
      mtlLoader.setPath( _url_files_blender );
      var url = "pared.mtl";
      mtlLoader.load( url, function( materials ) {

          materials.preload();
          materials.side = THREE.DoubleSide;
          materials.shadowSide = THREE.DoubleSide;
          console.log("materials", materials );
          if(materials.materials){
            for(let i in materials.materials){
              let mat = materials.materials[i];
              mat.side = THREE.DoubleSide;
              mat.shadowSide = THREE.DoubleSide;
            }
          }

          var objLoader = new OBJLoader();
          objLoader.setMaterials( materials );
          objLoader.setPath( _url_files+'/blender/' );
          objLoader.load( 'pared.obj', function ( object ) {
              object.castShadow = true;
              object.receiveShadow = true;
              //scene.add( object );
              /*
              object = object.clone();
              object.castShadow = true;
              object.receiveShadow = true;
              object.position.set(0,0,-10);
              scene.add( object );
              */

          }, function(msg){console.log(msg);}, function(msg){console.log(msg);} );

      });

      //window.setInterval(instance.fireBullets,500);
      this.loadWaveFrontObj(_url_files_blender + 'block.obj',instance.parseCollisionLimits);

    }
    parseCollisionLimits(data){
      let idx,idy,idz,object,obj,vertices,normals,faces,face,vertex,normal,triangle,geometry,wireframe,mesh,three_face
      let objects = data.objects,points 
      let triangleA,triangleB
      let planes = []
      let sphere,material,three_plane,counter = 0
      if(Array.isArray(objects)){
        for(idx in objects){
          let three_vertices = []
          let three_triangles = []
          obj = objects[idx]
          vertices  = obj.vertices
          faces     = obj.faces
          normals   = obj.normals
          vertices.map(function( vertex ){
            three_vertices.push( new THREE.Vector3( vertex[0] , vertex[1] , vertex[2] ) )
          })
          console.log("three_vertices")
          console.log(three_vertices)

          for( idy = 0 ; idy < faces.length; idy+=1){
            face  = faces[idy]
            three_plane = new THREE.Plane()
            three_plane.custom_name = "plane "+counter
            counter++
            three_plane.boundingSphere = new THREE.Sphere()
            points = []
            triangleA = new THREE.Triangle(
              three_vertices[face[0].vertex],
              three_vertices[face[1].vertex],
              three_vertices[face[3].vertex]
            )
            triangleB = new THREE.Triangle(
              three_vertices[face[1].vertex],
              three_vertices[face[2].vertex],
              three_vertices[face[3].vertex]
            )

            three_plane.pointA = three_vertices[face[0].vertex]
            three_plane.pointB = three_vertices[face[1].vertex]
            three_plane.pointC = three_vertices[face[2].vertex]
            three_plane.pointD = three_vertices[face[3].vertex]

            three_plane.lineA = new THREE.Line3( three_plane.pointA , three_plane.pointB )
            three_plane.lineB = new THREE.Line3( three_plane.pointB , three_plane.pointD )
            three_plane.lineC = new THREE.Line3( three_plane.pointD , three_plane.pointC )
            three_plane.lineD = new THREE.Line3( three_plane.pointC , three_plane.pointA )

            three_plane.center = new THREE.Vector3()
            three_plane.center.x = ( three_plane.pointA.x + three_plane.pointB.x + three_plane.pointC.x + three_plane.pointD.x ) / 4 
            three_plane.center.y = ( three_plane.pointA.y + three_plane.pointB.y + three_plane.pointC.y + three_plane.pointD.y ) / 4 
            three_plane.center.z = ( three_plane.pointA.z + three_plane.pointB.z + three_plane.pointC.z + three_plane.pointD.z ) / 4 

            three_plane.boundingSphere.setFromPoints([
                three_plane.pointA,
                three_plane.pointB,
                three_plane.pointC,
                three_plane.pointD,
              ])
            three_plane.boundingSphere.radius *= 1.5

            points.push( triangleA.a.x , triangleA.a.y , triangleA.a.z )
            points.push( triangleA.b.x , triangleA.b.y , triangleA.b.z )
            points.push( triangleA.c.x , triangleA.c.y , triangleA.c.z )

            points.push( triangleB.a.x , triangleB.a.y , triangleB.a.z )
            points.push( triangleB.b.x , triangleB.b.y , triangleB.b.z )
            points.push( triangleB.c.x , triangleB.c.y , triangleB.c.z )

            three_plane.triangleA = triangleA
            three_plane.triangleB = triangleB

            geometry = new THREE.BufferGeometry()
            
            points = new Float32Array( points );
            
            geometry.addAttribute( 'position', new THREE.BufferAttribute( points, 3 ) )

            wireframe = new THREE.WireframeGeometry( geometry )

            mesh = new THREE.LineSegments( wireframe )
            mesh.material.color.setRGB(1,1,1)
            mesh.material.depthTest = false
            mesh.material.opacity = 0.25
            mesh.material.linewidth = 10
            mesh.material.transparent = true
            
            three_plane.mesh_solid = mesh 
            
            scene.add( mesh )

            geometry = new THREE.SphereBufferGeometry( three_plane.boundingSphere.radius , 14, 14 )
            wireframe = new THREE.WireframeGeometry( geometry );

            mesh = new THREE.LineSegments( wireframe )
            mesh.material.color.setRGB(1,1,1)
            mesh.material.depthTest = false
            mesh.material.opacity = 0.25
            mesh.material.linewidth = 1
            mesh.material.transparent = true
            mesh.position.copy( three_plane.boundingSphere.center )
            
            three_plane.mesh_wireframe = mesh 

            scene.add( mesh )
            
            three_plane.setFromCoplanarPoints( triangleA.a , triangleA.b , triangleA.c )
            var helper = new THREE.PlaneHelper( three_plane, 10, 0xffff00 );
            scene.add( helper );
            var arrowHelper = new THREE.ArrowHelper( three_plane.normal,three_plane.pointA,10, 0xff0000, 2, 2 );
            scene.add( arrowHelper );
            var arrowHelper = new THREE.ArrowHelper( three_plane.normal,three_plane.pointB,10, 0x00ff00, 2, 2 );
            scene.add( arrowHelper );
            var arrowHelper = new THREE.ArrowHelper( three_plane.normal,three_plane.pointC,10, 0x0000ff, 2, 2 );
            scene.add( arrowHelper );
            var arrowHelper = new THREE.ArrowHelper( three_plane.normal,three_plane.pointD,10, 0x00ffff, 2, 2 );
            scene.add( arrowHelper );
            var arrowHelper = new THREE.ArrowHelper( three_plane.normal,three_plane.center,10, 0xffffff, 2, 2 );
            scene.add( arrowHelper );
            planes.push( three_plane )
          }
        }
      }
      sphere = new THREE.Sphere()
      sphere.collisionables = []
      points = []
      for(idx in planes){
        three_plane = planes[idx]
        sphere.collisionables.push(three_plane)
        points.push(
          three_plane.pointA,
          three_plane.pointB,
          three_plane.pointC,
          three_plane.pointD,
        )
      }
      sphere.setFromPoints(points)
      sphere.radius *= 1.5

      collisionables.push(sphere)

      geometry = new THREE.SphereBufferGeometry( sphere.radius , 32 , 32 )
      wireframe = new THREE.WireframeGeometry( geometry );

      mesh = new THREE.LineSegments( wireframe )
      mesh.material.color.setRGB(0,1,0)
      mesh.material.depthTest = false
      mesh.material.opacity = 0.25
      mesh.material.linewidth = 1
      mesh.material.transparent = true
      mesh.position.copy( sphere.center )
      scene.add( mesh )
      console.log( "collisionables" , collisionables )
      cameraUtils.setCollisionables( collisionables )
    }
    loadWaveFrontObj(str,cb){

      axios.get(str,{ 
        headers:{  
        },
        params:{ 
        }, 
      }).then(
      function(response){
        if(dev_mode)console.log(response);

        //let parsedJSON = parseWFObj(response.data);
        let parsedJSON = objParser(response.data,cb);
        console.log(parsedJSON);
      }).catch(function(err){
        if(dev_mode){
          console.log(err);
          console.log(err.response);
          console.log(err.status);
        }
      }); 
    }
    validateBulletPosition(bullet){
      return (bullet.position.x >= -200 && bullet.position.x <= 200) && 
            (bullet.position.y >= -34 && bullet.position.y <= 34) && 
            (bullet.position.z >= -350 && bullet.position.z <= 350);
    }
    moveBullets(){
      for( let idx in lightBulletsInstances ){

        let lightBullet_tmp = lightBulletsInstances[ idx ];

        if( lightBullet_tmp.visible ){

          lightBullet_tmp.position_prev.copy( lightBullet_tmp.position );

          lightBullet_tmp.position.addScaledVector( lightBullet_tmp.dir , 3 );

          lightBullet_tmp.line3.start.copy( lightBullet_tmp.position_prev );

          lightBullet_tmp.line3.end.copy( lightBullet_tmp.position );

          lightBullet_tmp.boundingSphere.center.copy( lightBullet_tmp.position );

          if( !instance.validateBulletPosition( lightBullet_tmp) ){

            lightBullet_tmp.visible = false;

          }
        }
      }
    }
    setIntervalBullets(){
      if(intervalBulletCount <=0){
        intervalBulletCount = intervalBulletCountDef;
        instance.fireBullets();
      }
      intervalBulletCount--;
    }
    loop(){
      instance.moveBullets();

      instance.setIntervalBullets();

      instance.detectCollisionsWithBullets();
    }

    quadContainsPoint(plane,point){
      return plane.triangleA.containsPoint(point) || plane.triangleB.containsPoint(point);
    }
    setBounceBullet(bullet,point_intersection,plane){

      bullet.position.copy( point_intersection ).addScaledVector( bullet.dir , - bullet.boundingSphere.radius - 0.5 )
      
      bullet.position_prev.copy( bullet.position )

      bullet.line3.start.copy( bullet.position_prev );

      bullet.line3.end.copy( bullet.position );

      bullet.boundingSphere.center.copy( bullet.position );
      
      bullet.dir.reflect( plane.normal )

    }
    moveBackBulletToNearest(bullet,point_intersection,plane){

      let d4 = point_intersection.distanceTo( bullet.position_prev )

      let d1 = plane.distanceToPoint( bullet.position_prev )

      bullet.position.copy( point_intersection ).addScaledVector( bullet.dir , - ( bullet.boundingSphere.radius + 0.1 )*( d4 / d1 ) )

      bullet.dir.reflect( plane.normal )

    }
    quadEdgesPointsCollidesWithBullet( bullet , plane ){
      let distanceA,distanceB,distanceC,distanceD
      distanceA = plane.pointA.distanceTo( bullet.position )
      distanceB = plane.pointB.distanceTo( bullet.position )
      distanceC = plane.pointC.distanceTo( bullet.position )
      distanceD = plane.pointD.distanceTo( bullet.position )

      if( 

        distanceA <= bullet.boundingSphere.radius || 
        distanceB <= bullet.boundingSphere.radius || 
        distanceC <= bullet.boundingSphere.radius || 
        distanceD <= bullet.boundingSphere.radius

      ){
        return true
      }
      return false

    }
    detectCollisionsWithBullets(){
      let idx,idy,idz,lightBullet_tmp,tmp;

      lightBulletsInstances.map( function( bullet ){
          
        if( bullet.visible  ){

          collisionables.map( function( collisionable ){

            if( bullet.visible  ){

              if( bullet.mesh_bullet.geometry.boundingSphere.intersectsSphere( collisionable ) ){

                if( collisionable.collisionables ){

                  collisionable.collisionables.map(function( plane ){

                    if( bullet.visible  ){

                      if( bullet.boundingSphere.intersectsSphere( plane.boundingSphere ) ){

                        if( plane.intersectLine(  bullet.line3 , tmp_vector3 ) ){

                          if( tmp_vector3.distanceTo( bullet.line3.start ) <= bullet.line3.distance() ){

                            if( instance.quadContainsPoint (plane,tmp_vector3 ) ){

                              instance.setBounceBullet(bullet,tmp_vector3,plane)
                              //bullet.visible = false;

                            }else if( false ){
                              //if bounding sphere of bullet at tmp_vector3 collide with points or edges of the plane
                            }else if( instance.quadEdgesPointsCollidesWithBullet( bullet , plane ) ){

                              instance.moveBackBulletToNearest( bullet , tmp_vector3 , plane )

                            }else{

                            }

                          }

                        }

                      }

                    }

                  });

                }

              }

            }

          });

        }

      });
    }
    onDocumentMouseMove(evt){
      mouse2D.x = 0;
      mouse2D.y = 0;
    }
    onDocumentMouseDown(evt){
      isMouseDown = true;
    }
    onDocumentMouseUp(evt){
      isMouseDown = false;
    }
    onDocumentKeyDown(evt){
      switch ( event.keyCode ) {
        case 16: isShiftDown = true; break;
        case 17: isCtrlDown = true; break;
      }
    }
    onDocumentKeyUp(evt){
      switch ( event.keyCode ) {
        case 16: isShiftDown = false; break;
        case 17: isCtrlDown = false; break;
      }
    }
    putObjectsFromJson(json){
      if(cubeMaterial){
        instance.addVoxelFromJson(json);
      }else{
        pendingJson = json;
      }
    }
    fireBullets(){
      if(isMouseDown){
        let light_temp,light_mesh_bullet;
        for(let idx in lightBulletsInstances){
          let lightBullet_tmp = lightBulletsInstances[idx];
          if( 
            !lightBullet_tmp.visible
          ){
            light_temp = lightBullet_tmp;
            break;
          }
        }
        if(!light_temp){
          
          light_temp = lightBullet.clone();

          light_mesh_bullet = new THREE.Mesh( sphereBullet, bulletMaterial );

          light_mesh_bullet.geometry.computeBoundingSphere();

          light_temp.boundingSphere = light_mesh_bullet.geometry.boundingSphere.clone();

          light_temp.mesh_bullet = light_mesh_bullet;

          light_temp.add( light_mesh_bullet );
          
          light_temp.position_prev = new THREE.Vector3();
          light_temp.line3 = new THREE.Line3();

          scene.add( light_temp );
          
          lightBulletsInstances.push( light_temp );
        }
        light_temp.visible = true;
        light_temp.position.copy( cameraUtils.getEyePoint( true ) );
        light_temp.position_prev.copy( light_temp.position );
        light_temp.line3.start.copy( light_temp.position_prev );
        light_temp.line3.end.copy( light_temp.position );
        light_temp.boundingSphere.center.copy( light_temp.position );
        light_temp.dir = cameraUtils.getLookAtPoint( true ).clone().sub( cameraUtils.getEyePoint( true ) ).normalize();
      }
    }

  }

  return {
    getInstance(obj){
      if(!instance || dev_mode || true){
        instance = new World(obj);
        return instance;
      }else{
        return null;
      }
    }
  };
})();

export default func;