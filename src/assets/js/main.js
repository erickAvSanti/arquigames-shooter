const THREE = require('three');
const jQuery = require('jquery');
const constants = require('./constants.js');
import CameraUtils from "@/assets/js/cameraUtils.js";
import World from "@/assets/js/world.js";


function init(_vue){
	let instance = null;
	let scene,camera,renderer,element;



	var cube,plane;
	var code = Math.random();
	var loopers = [];

	let cameraUtils = null;
	let debug = false;

	let duration = 500;
  let currentTime = Date.now();

  let runAnimation = true;
  let world = null;

  let key = {
    frontParalelKey : false,
    backParalelKey : false,
    leftKey : false,
    rightKey : false,
    topKey : false,
    bottomtKey : false,
    frontParalelKey : false,
    backParalelKey : false,
    veloc_plus : false,
  };

  let rotView = {x:0,y:0};

	const dev_mode = process.env.NODE_ENV === 'development';

	let _keydown_callback = null;
	let _keyup_callback = null;
	let _resize_callback = null;

	class CApp{
		constructor(_vue){
			this.createThree();
			cameraUtils = CameraUtils.getInstance(this);
			world = World.getInstance(this);
			this.hasLocked = false;
			this.selected_color = "#ff0000";
			this.vue_container = _vue;
			this.objects_set = {};

			_keyup_callback = false;
			_keydown_callback = false;
		}
		createThree(){
			if(!renderer){
				scene = new THREE.Scene();
				camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 10000 );
				
				camera.position.set( constants.position.x,constants.position.y,constants.position.z );
				camera.up.set( constants.up.x,constants.up.y,constants.up.z );
				camera.lookAt(new THREE.Vector3(constants.lookAt.x,constants.lookAt.y,constants.lookAt.z));
				renderer = new THREE.WebGLRenderer({antialias: false});
			
				renderer.shadowMap.enabled = true;

				renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				element = renderer.domElement;
				jQuery("#canvas_container").html(element);
			}
		}
		getCameraUtils(){
			return cameraUtils;
		}
		reset(){
			currentTime = Date.now();
		}
		setBackground(opt,value){
			if( opt=='b' )scene.background = new THREE.Color(value);
		}
		init() {
			var geometry, material;
			geometry = new THREE.BoxGeometry( 1, 1, 1 );
			material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
			if(debug){
				cube = new THREE.Mesh( geometry, material );
				scene.add( cube );
			} 
			var ambientLight = new THREE.AmbientLight( 0x606060 );
			scene.add( ambientLight );
			/*
			var directionalLight = new THREE.DirectionalLight( 0xffffff );
			directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
			directionalLight.castShadow = true;
			directionalLight.shadow.mapSize.width = 2048;  // default
			directionalLight.shadow.mapSize.height = 2048; // default
			directionalLight.shadow.camera.near = 0.5;    // default
			directionalLight.shadow.camera.far = 2048;     // default
			scene.add( directionalLight );
			*/
			cameraUtils.ready = true;
			if(!_keydown_callback){
				_keydown_callback = function(evt){
					let kk = evt.key.toLowerCase();
					if( evt.code=='KeyW' || evt.keyCode==87 || evt.which==87 || evt.key=='w' )key.backParalelKey 	= true;
					if( evt.code=='KeyS' || evt.keyCode==83 || evt.which==83 || evt.key=='s' )key.frontParalelKey	= true;
					if( evt.code=='KeyA' || evt.keyCode==65 || evt.which==65 || evt.key=='a' )key.leftKey 				= true;
					if( evt.code=='KeyD' || evt.keyCode==68 || evt.which==68 || evt.key=='d' )key.rightKey 				= true;
					//if( evt.code=='KeyQ' || evt.keyCode==81 || evt.which==81 || evt.key=='q' )key.topKey 					= true;
					//if( evt.code=='KeyE' || evt.keyCode==69 || evt.which==69 || evt.key=='e' )key.bottomtKey 			= true;
					if( evt.code=='KeyE' || evt.keyCode==69 || evt.which==69 || evt.key=='e' )key.veloc_plus 			= true;
				};
				jQuery(window).keydown(_keydown_callback);
			}
			if(!_keyup_callback){
				_keyup_callback = function(evt){
					if( evt.code=='KeyW' || evt.keyCode==87 || evt.which==87 || evt.key=='w' )key.backParalelKey 	= false;
					if( evt.code=='KeyS' || evt.keyCode==83 || evt.which==83 || evt.key=='s' )key.frontParalelKey	= false;
					if( evt.code=='KeyA' || evt.keyCode==65 || evt.which==65 || evt.key=='a' )key.leftKey 				= false;
					if( evt.code=='KeyD' || evt.keyCode==68 || evt.which==68 || evt.key=='d' )key.rightKey 				= false;
					//if( evt.code=='KeyQ' || evt.keyCode==81 || evt.which==81 || evt.key=='q' )key.topKey 					= false;
					//if( evt.code=='KeyE' || evt.keyCode==69 || evt.which==69 || evt.key=='e' )key.bottomtKey 			= false;
					if( evt.code=='KeyE' || evt.keyCode==69 || evt.which==69 || evt.key=='e' )key.veloc_plus 			= false;
	        //if( ( evt.code=='KeyB' || evt.keyCode==66 || evt.which==66 || evt.key=='b' ) && instance.vue_container && instance.vue_container.modals && !instance.vue_container.modals.reg)runAnimation = !runAnimation;
				};
				jQuery(window).keyup(_keyup_callback);
			}
			this.resetLoopers();
			if(world){
				world.init();
				this.addLooper(world.loop);
			}
			if(cameraUtils){
				this.addLooper(cameraUtils.loop);
			}

		}
		addEvents(){
			jQuery(window).resize(function(evt){instance.resized = true;});
		}
		resetLoopers(){
			loopers.splice(0,loopers.length);
		}
		addLooper(_looper){
			if(!loopers.includes(_looper))loopers.push(_looper);
		}
		getCamera(){
			return camera;
		}
		getScene(){
			return scene;
		}
		getRenderer(){
			return renderer;
		}
		rotateView(x,y){
			rotView.x = x;
			rotView.y = y;
		}
		getElement(){
			return element;
		}
		putObjects(json){
		}
	};


	function animate() {
		window.req_anim = window.requestAnimationFrame( animate );
	  let now = Date.now();
	  let deltatime = now - currentTime;
	  currentTime = now;
	  let fraction = deltatime / duration;
		if( runAnimation ){

			if(debug){
				cube.rotation.x += 0.01;
				cube.rotation.y += 0.01;
			}

			if(cameraUtils){
				cameraUtils.savePreviousValues();
				if(rotView.x != 0 || rotView.y != 0){
					cameraUtils.rotateView(rotView.x,rotView.y);
					rotView.x = 0;
					rotView.y = 0;
				}
				if(instance.vue_container.share_mode || (instance.vue_container && !instance.vue_container.modals.reg)){
			 		
			 		cameraUtils.moveView(
			      key.frontParalelKey,
			      key.backParalelKey,
			      key.leftKey,
			      key.rightKey,
			      key.topKey,
			      key.bottomtKey,
			      key.frontParalelKey,
			      key.backParalelKey,
			      key.veloc_plus,
			      fraction
			    );
				}
				for(var idx in loopers){
					loopers[idx](fraction);
				}  
			}
			renderer.render( scene, camera );
		}
		if(instance.resized){
			resize();
			instance.resized = false;
		}
	}
	function resize(){
		camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
	}

	if(!instance){
		instance = new CApp(_vue);
		instance.init();
		animate();
		instance.addEvents();
	}
	return instance;

}
export {init};