const jQuery = require('jquery');
const THREE = require('three');
const glm = require('gl-matrix');
const constants = require('./constants.js');


const func = (function(){

  let instance  = null;
  let capp      = null;


  let lookAtPoint = glm.vec3.create();
  let lookAtPoint_prev = glm.vec3.create();
  let upVector    = glm.vec3.create();
  let eyePoint    = glm.vec3.create();
  let eyePoint_prev    = glm.vec3.create();

  let velocityI_W = 0;
  let velocityF_W = 0;
  let acceleration_W = 0.3;
  let velocityI_S = 0;
  let velocityF_S = 0;
  let acceleration_S = 0.3;
  let velocityI_Q = 0;
  let velocityF_Q = 0;
  let acceleration_Q = 0.1;
  let velocityI_Z = 0;
  let velocityF_Z = 0;
  let acceleration_Z = 0.1;
  let distance_W = 0;
  let distance_S = 0;
  let distance_Z = 0;
  let distance_Q = 0;
  let currentTime = Date.now();

  let velocity_constant_def = 3;
  let velocity_constant_plus = 5;
  let velocity_constant = velocity_constant_def;
  let use_velocity_constant = true;
  const dev_mode = process.env.NODE_ENV === 'development';

  let _keyup_callback   = null;
  let _keydown_callback = null;

  let key = {
    jump:false,
  }
  let jumping = {
    vAngle : 0,
    speed:0.15,
    gravityY:-20,
    velocityYf:0,
    velocityY0:20,
    velocityY0_def:20,
    default_action:false,
  }



  let const1 = Math.PI*180/360;

  let cameraLookAtPoint         = new THREE.Vector3();
  let cameraLookAtPoint_prev    = new THREE.Vector3();
  let cameraEyePoint            = new THREE.Vector3();
  let cameraEyePoint_prev       = new THREE.Vector3();
  let tmpVector3                = new THREE.Vector3();
  let tmpVector3_2              = new THREE.Vector3();
  let tmpVector3_3              = new THREE.Vector3();
  let spotLight,spotLightHelper;
  let scene,camera;

  let boundingSphere = new THREE.Sphere(new THREE.Vector3(),constants.cameraBoundingRadius);
  let boundingSphere_prev = new THREE.Sphere(new THREE.Vector3(),constants.cameraBoundingRadius);

  let line3 = new THREE.Line3();
  line3.dir = new THREE.Vector3();

  let direction = new THREE.Vector3();


  let frontDirection = glm.vec3.create();
  let strafeDirection = glm.vec3.create();
  let lookAtPoint_tmp = glm.vec3.create();
  let tmp_vec3_1 = glm.vec3.create();
  let tmp_vec3_2 = glm.vec3.create();
  let q = glm.quat.create();

  let factor_halfPI = 180/Math.PI;

  let collisionables = [];

  class CameraUtils{
    constructor(_capp){
      capp = _capp;
      this.ready = false;
      /*
      if(!_keyup_callback){
        _keyup_callback = function(evt){
          if( evt.code=='KeySpace' || evt.keyCode==32 || evt.which==32 || evt.key=='w' )key.jump  = true;
        };
        jQuery(window).keyup(_keyup_callback);
      }
      */


    }
    init(){
      instance.ready = false;
      currentTime = Date.now();
      lookAtPoint = glm.vec3.fromValues(constants.lookAt.x,constants.lookAt.y,constants.lookAt.z);
      upVector    = glm.vec3.fromValues(constants.up.x,constants.up.y,constants.up.z);
      eyePoint    = glm.vec3.fromValues(constants.position.x,constants.position.y,constants.position.z);
    
      if(!_keydown_callback){
        _keydown_callback = function(evt){
          let kk = evt.key.toLowerCase();
          if( evt.code=='KeySpace' || evt.keyCode==32 || evt.which==32 || evt.key=='space' )key.jump  = true;
        };
        jQuery(window).keydown(_keydown_callback);
      }
      scene = capp.getScene();
      camera = capp.getCamera();
      instance.setIlumination();
    }
    rotateViewAround(deltaAngle, axis) {
      if(!instance.ready)return;
      glm.vec3.subtract(frontDirection, lookAtPoint_tmp, eyePoint);
      glm.vec3.normalize(frontDirection , frontDirection);
      glm.quat.setAxisAngle(q, axis, deltaAngle);
      glm.vec3.transformQuat(frontDirection, frontDirection, q);
      glm.vec3.normalize(frontDirection , frontDirection);
      let angle = glm.vec3.angle(frontDirection, upVector)*factor_halfPI;
      if( angle < 176 && angle > 4 ){
        glm.vec3.copy(lookAtPoint_tmp,eyePoint);
        glm.vec3.add(lookAtPoint_tmp, lookAtPoint_tmp, frontDirection);  
      }
    }
    rotateView(x, y) {  
      if(!instance.ready)return;
      glm.vec3.copy(lookAtPoint_tmp,lookAtPoint);
      glm.vec3.subtract(frontDirection, lookAtPoint_tmp, eyePoint);
      glm.vec3.normalize(frontDirection,frontDirection);
      glm.vec3.cross(strafeDirection, frontDirection, upVector);
      glm.vec3.normalize(strafeDirection,strafeDirection);
      this.rotateViewAround(-x/360.0, upVector); 
      this.rotateViewAround(-y/360.0, strafeDirection);
      glm.vec3.copy(lookAtPoint, lookAtPoint_tmp);           
    }
    moveView(w,s,a,d,q,e,z,c,speed_flag, dt) {  
      if(!instance.ready)return;
      if(speed_flag){
        velocity_constant = velocity_constant_plus;
      }else{
        velocity_constant = velocity_constant_def;
      }
      let frontDirection = glm.vec3.create();
      let strafeDirection = glm.vec3.create();
      let frontParalelDirection = glm.vec3.create();
      glm.vec3.set(frontParalelDirection,0,1,0);
      let strafeParalelDirection = glm.vec3.create();
      glm.vec3.subtract(frontDirection, lookAtPoint, eyePoint);
      glm.vec3.normalize(frontDirection, frontDirection);
      glm.vec3.cross(strafeDirection, frontDirection, upVector);
      glm.vec3.normalize(strafeDirection, strafeDirection);
      glm.vec3.copy(strafeParalelDirection, strafeDirection); 

      let forwardScale = 0.0;
      let strafeScale = 0.0;
      let topScale = 0.0;
      let frontParalelScale = 0.0;


      if (w) {
        //forwardScale += 1.0; 
        frontParalelScale += 1.0;
      }
      if (s) {
        //forwardScale -= 1.0; 
        frontParalelScale -= 1.0;
        
      }
      if (a) {
        strafeScale -= 1.0;
      }
      if (d) {
        strafeScale += 1.0;
      } 
      if (q) {
        topScale += 1.0;
      }
      if (e) {
        topScale -= 1.0;
      }
      if (z) {
        frontParalelScale += 1.0;
      }
      if (c) {
        frontParalelScale -= 1.0;
      } 
      if(forwardScale==0){
        velocityI_W=0;
        velocityF_W=0;
        if(acceleration_W<0){
          acceleration_W = - acceleration_W;
        }
      }
      if(strafeScale==0){
        velocityI_S=0;
        velocityF_S=0;
        if(acceleration_S<0){
          acceleration_S = - acceleration_S;
        }
      }
      if(topScale==0){
        velocityI_Q=0;
        velocityF_Q=0;
        if(acceleration_Q<0){
          acceleration_Q = - acceleration_Q;
        }
      }
      if(frontParalelScale==0){
        velocityI_Z=0;
        velocityF_Z=0;
        if(acceleration_Z<0){
          acceleration_Z = - acceleration_Z;
        }
      }


      let timeVariation = 4.0 * dt; 
      /*
      forwardScale *= timeVariation;
      strafeScale *= timeVariation;
      */
      if(capp.hasLocked===true && !use_velocity_constant){
        /*calculo de la ditancia a avanzar utilizando las velocidades y la aceleracion*/
        velocityI_W  = velocityF_W;
        velocityF_W  = velocityI_W + acceleration_W*timeVariation;
        distance_W   = velocityI_W*timeVariation + 0.5*acceleration_W*timeVariation*timeVariation;

        forwardScale *= distance_W;

        /*********************************************************************************************/

        velocityI_S  = velocityF_S;
        velocityF_S  = velocityI_S + acceleration_S*timeVariation;
        distance_S   = velocityI_S*timeVariation + 0.5*acceleration_S*timeVariation*timeVariation;

        strafeScale *= distance_S;

        /*********************************************************************************************/

        velocityI_Q  = velocityF_Q;
        velocityF_Q  = velocityI_Q + acceleration_Q*timeVariation;
        distance_Q   = velocityI_Q*timeVariation + 0.5*acceleration_Q*timeVariation*timeVariation;

        topScale *= distance_Q;

        /*********************************************************************************************/

        velocityI_Z  = velocityF_Z;
        velocityF_Z  = velocityI_Z + acceleration_Z*timeVariation;
        distance_Z   = velocityI_Z*timeVariation + 0.5*acceleration_Z*timeVariation*timeVariation;

        frontParalelScale *= distance_Z;
      }else{
        forwardScale      *= timeVariation * velocity_constant;
        strafeScale       *= timeVariation * velocity_constant;
        topScale          *= timeVariation * velocity_constant;
        frontParalelScale *= timeVariation * velocity_constant;
      }
      /*console.log("forwardScale: "+forwardScale);
      console.log("strafeScale: "+strafeScale);
      console.log("topScale: "+topScale);
      console.log("frontParalelScale: "+frontParalelScale);*/
      


      glm.vec3.scale(frontDirection, frontDirection, forwardScale);
      glm.vec3.scale(strafeDirection, strafeDirection, strafeScale);

      glm.vec3.add(eyePoint, eyePoint, frontDirection);
      glm.vec3.add(eyePoint, eyePoint, strafeDirection);
      glm.vec3.add(lookAtPoint, lookAtPoint, frontDirection);
      glm.vec3.add(lookAtPoint, lookAtPoint, strafeDirection);

        
      eyePoint[1] += topScale;
      lookAtPoint[1] += topScale;





      let q1 = glm.quat.create();
      glm.quat.setAxisAngle(q1, strafeParalelDirection, const1);
      glm.vec3.transformQuat(frontParalelDirection, frontParalelDirection, q1);
      glm.vec3.scale(frontParalelDirection, frontParalelDirection, frontParalelScale); 

      glm.vec3.add(eyePoint, eyePoint, frontParalelDirection); 
      glm.vec3.add(lookAtPoint, lookAtPoint, frontParalelDirection); 
    }
    setEyePoint(three_vector3){
      eyePoint[0] = three_vector3.x;
      eyePoint[1] = three_vector3.y;
      eyePoint[2] = three_vector3.z;
    }
    setUpVector(three_vector3){
      upVector[0] = three_vector3.x;
      upVector[1] = three_vector3.y;
      upVector[2] = three_vector3.z;
    }
    getLookAtPoint(three_version = false){
      if(three_version){
        return cameraLookAtPoint.set(lookAtPoint[0],lookAtPoint[1],lookAtPoint[2]);
      }else{
        return lookAtPoint;
      }
    }
    getEyePoint(three_version = false){
      if(!eyePoint || eyePoint.length<3)console.log("eyePoint empty",eyePoint);
      if(three_version){
        return cameraEyePoint.set(eyePoint[0],eyePoint[1],eyePoint[2]);
      }else{
        return eyePoint;
      }
    }
    loop(fraction_time){
      if(key.jump){
        if(jumping.default_action){
          jumping.vAngle += jumping.speed;
          let val = Math.sin(jumping.vAngle) ;
          eyePoint[1] += val;
          lookAtPoint[1] += val;

          if (eyePoint[1] <= constants.position.y) {
            eyePoint[1] = constants.position.y;
            //lookAtPoint[1] = constants.position.y;
            jumping.vAngle = 0;
            key.jump = false;
          }else{
          }
        }else{
          jumping.velocityYf = jumping.velocityY0 + jumping.gravityY*fraction_time
          let val = ( Math.pow(jumping.velocityYf,2) - Math.pow(jumping.velocityY0,2) ) /( 2 * jumping.gravityY )
          
          eyePoint[1] += val;
          lookAtPoint[1] += val;

          jumping.velocityY0 = jumping.velocityYf;

          if (eyePoint[1] < constants.position.y) {
            let diffY = constants.position.y - eyePoint[1];
            eyePoint[1] = constants.position.y;
            lookAtPoint[1] += diffY;
            jumping.velocityYf = 0;
            jumping.velocityY0 = jumping.velocityY0_def ;
            key.jump = false;
          }else{
          }

        }

      }
      cameraEyePoint.set( eyePoint[0] , eyePoint[1] , eyePoint[2] );
      cameraLookAtPoint.set( lookAtPoint[0] , lookAtPoint[1] , lookAtPoint[2] );
      
      camera.position.copy( cameraEyePoint );
      camera.lookAt( cameraLookAtPoint );
      boundingSphere.center.copy( cameraEyePoint );

      line3.start.copy( cameraEyePoint_prev )
      line3.end.copy( cameraEyePoint )
      line3.dir.copy( line3.end ).sub( line3.start ).normalize()

      instance.detectCollisions( collisionables )
 

      if(spotLight){
        spotLight.position.copy(cameraEyePoint);
        spotLight.target.position.copy(cameraLookAtPoint.clone().sub(cameraEyePoint).normalize().multiplyScalar(1000).add(cameraEyePoint));
      }
      //spotLightHelper.update();

    }
    setCollisionables( cc ){
      collisionables = cc
    } 
    savePreviousValues(){

      cameraLookAtPoint_prev.copy(cameraLookAtPoint);
      cameraEyePoint_prev.copy(cameraEyePoint);
      boundingSphere_prev.center.copy(cameraEyePoint);

      glm.vec3.copy(eyePoint_prev,eyePoint);
      glm.vec3.copy(lookAtPoint_prev,lookAtPoint);
    }
    setPositionFromThreeVector3(v){
      glm.vec3.set( eyePoint , v.x , v.y , v.z )
      glm.vec3.set( eyePoint_prev , v.x , v.y , v.z )
      
      cameraEyePoint.set( v.x , v.y , v.z )
      cameraEyePoint_prev.set( v.x , v.y , v.z )

      boundingSphere.center.copy( v )
      boundingSphere_prev.center.copy( v )

      line3.start.copy( cameraEyePoint )
      line3.end.copy( cameraEyePoint )
    }
    setIlumination(){
      console.log("setIlumination");
      spotLight = new THREE.SpotLight( 0xffffff,1,80,0.69,0.1,2 );
      spotLight.position.set(0,20,25);
      spotLight.castShadow = true;            // default false
      scene.add( spotLight );
      scene.add( spotLight.target );
      //spotLightHelper = new THREE.SpotLightHelper( spotLight );
      //scene.add( spotLightHelper );

      //Set up shadow properties for the light
      spotLight.shadow.mapSize.width = 2048;  // default
      spotLight.shadow.mapSize.height = 2048; // default
      spotLight.shadow.camera.near = 0.5;       // default
      spotLight.shadow.camera.far = 20      // default
      spotLight.shadow.radius = 8; 
    }
    collisionWithSphere(obj){
      return boundingSphere.intersectsSphere(obj instanceof THREE.Sphere ? obj : obj.boundingSphere);
    }
    collideWithPlane(plane){
      let distance = plane.distanceToPoint(boundingSphere.center);
      return distance <= boundingSphere.radius;
    }
    projectCenterIntoPlane(plane){
      let target = new THREE.Vector3();
      plane.projectPoint(boundingSphere.center,target);
      return target;
    }
    projectPreviousCenterIntoPlane(plane){
      let target = new THREE.Vector3();
      plane.projectPoint(boundingSphere_prev.center,target);
      return target;
    }
    getDistanceToMoveCircleOrSphereToPreventCollisionWithQuad(plane,sign){
      
      let a, b, m, distance;

      b = plane.distanceToPoint(boundingSphere_prev.center);
      a = plane.distanceToPoint(boundingSphere.center);
      m = boundingSphere_prev.center.distanceTo( boundingSphere.center );

      if(sign<0){
        distance = m * ( b - boundingSphere.radius ) / ( a + b )
      }else{
        distance = m * ( b - boundingSphere.radius ) / ( b - a )
      }
      return distance;
    }
    circleOrSphereIntersectsEdge(sphere,start_point,end_point){
      tmpVector3.copy(sphere.center).sub(start_point);
      tmpVector3_2.copy(end_point).sub(start_point);
      let k = tmpVector3.dot(tmpVector3_2);
      if( k > 0 ){
        let len = tmpVector3_2.length();
        k = k / len;
        if( k < len ){
          if( tmpVector3.length() <= sphere.radius ) return true;
        }
      }
      return false;
    }
    triangleCollidesWithCircleOrSphere(triangle,sphere){
      let d,sign1,sign2,sign3;
      d = triangle.a.distanceTo(sphere.center);
      if( d < sphere.radius )return true;
      d = triangle.b.distanceTo(sphere.center);
      if( d < sphere.radius )return true;
      d = triangle.c.distanceTo(sphere.center);
      if( d < sphere.radius )return true;
      tmpVector3_2.copy( sphere.center ).sub( triangle.c );
      sign1 = tmpVector3.copy( triangle.a ).sub( triangle.c ).dot( tmpVector3_2 );
      tmpVector3_2.copy( sphere.center ).sub( triangle.b );
      sign2 = tmpVector3.copy( triangle.c ).sub( triangle.b ).dot( tmpVector3_2 );
      tmpVector3_2.copy( sphere.center ).sub( triangle.a );
      sign3 = tmpVector3.copy( triangle.b ).sub( triangle.a ).dot( tmpVector3_2 );

      let flag = sign1>=0 && sign2>=0 && sign3>=0;
      if( flag )return true;

      flag = instance.circleOrSphereIntersectsEdge(sphere,triangle.c,triangle.a);
      if( flag )return true;
      flag = instance.circleOrSphereIntersectsEdge(sphere,triangle.b,triangle.c);
      if( flag )return true;
      flag = instance.circleOrSphereIntersectsEdge(sphere,triangle.a,triangle.b);
      if( flag )return true;

      return false;
    }
    quadContainsPoint(plane,point){
      return plane.triangleA.containsPoint(point) || plane.triangleB.containsPoint(point);
    }
    QuadCollideWithCircleOrSphere(plane,circle){
      return instance.triangleCollidesWithCircleOrSphere(plane.triangleA,circle) || instance.triangleCollidesWithCircleOrSphere(plane.triangleB,circle);
      //return plane.triangleA.containsPoint(point) || plane.triangleB.containsPoint(point);
    }
    QuadCollideWithCircleSphereProjected(plane,centerProjected,vectorFromCenterSphereToPointProjectedIntoPlane,distanceFromCenterSphereToPointProjectedIntoPlane){
      let tempRadius = Math.sqrt(Math.pow(boundingSphere.radius,2) - Math.pow(distanceFromCenterSphereToPointProjectedIntoPlane,2));
      let circle = new THREE.Sphere(centerProjected,tempRadius);
      return instance.QuadCollideWithCircleOrSphere(plane,circle);
    }
    collideWithQuad(plane){
      //plane.pointA
      //plane.pointB
      //plane.pointC
      //plane.pointD
      //plane.triangleA
      //plane.triangleB
      //plane.normal
      //plane.boundingSphere
      //plane.boundingSphere.center
      //plane.boundingSphere.radius
      direction.copy(boundingSphere.center).sub(boundingSphere_prev.center).normalize();
      let collide = false;
      if( instance.collisionWithSphere(plane) ){
        if( instance.collideWithPlane(plane) ){
          let centerProjected = instance.projectCenterIntoPlane(plane);
          tmpVector3.copy(boundingSphere.center).sub(centerProjected);
          let vectUnit = tmpVector3.normalize();
          let diff = boundingSphere.radius - tmpVector3.length();
          //let collide = instance.quadContainsPoint(plane,centerProjected);
          collide = instance.QuadCollideWithCircleSphereProjected(plane,centerProjected,vect,tmpVector3.length());
          if( collide ){
            let centerProjected_prev = instance.projectPreviousCenterIntoPlane(plane);
            let dist = instance.getDistanceToMoveCircleOrSphereToPreventCollisionWithQuad(plane,centerProjected.dot(centerProjected_prev));
            if(dist > 0.3){
              boundingSphere.center.addScaledVector(direction,dist);
            }else{
              let glm_vec3_tmp = glm.vec3.create();
              glm.vec3.subtract(glm_vec3_tmp,eyePoint,eyePoint_prev);
              tmpVector3.set(glm_vec3_tmp[0],glm_vec3_tmp[1],glm_vec3_tmp[2]);
              tmpVector3.projectOnPlane(plane.normal);
              glm.vec3.set(glm_vec3_tmp,tmpVector3.x,tmpVector3.y,tmpVector3.z);
              glm.vec3.add(eyePoint_prev,eyePoint_prev,glm_vec3_tmp);
            }
            /*
            */
          }
        }
      }
      return collide;
    }

    addjustPositionAfterCollision(previous_bounding_sphere_center , scalarToBack ,projectPoint , projectPoint_prev , thrut_projectPoint ){

      let v1 = previous_bounding_sphere_center.clone().sub( boundingSphere.center )
      let v2 = boundingSphere.center.clone().sub( boundingSphere_prev.center )

      let tmp_dir = projectPoint.clone().sub( thrut_projectPoint )
      let tmp_dir_distance = tmp_dir.length()
      tmp_dir.normalize()

      let same_side = false

      if( v1.dot(v2) >= 0 ){

        same_side = true
        boundingSphere.center.addScaledVector( tmp_dir , tmp_dir_distance )

      }else{
        
        //boundingSphere.center.copy( boundingSphere_prev.center )
        boundingSphere_prev.center.copy( boundingSphere.center )
        boundingSphere.center.addScaledVector( tmp_dir , tmp_dir_distance )
        //boundingSphere.center.copy( boundingSphere_prev.center )

        cameraEyePoint_prev.copy( boundingSphere_prev.center )

        eyePoint_prev[0] = cameraEyePoint_prev.x
        eyePoint_prev[1] = cameraEyePoint_prev.y
        eyePoint_prev[2] = cameraEyePoint_prev.z

      }

      cameraEyePoint.copy( boundingSphere.center )

      eyePoint[0] = boundingSphere.center.x
      eyePoint[1] = boundingSphere.center.y
      eyePoint[2] = boundingSphere.center.z

      camera.position.copy( cameraEyePoint )

      cameraLookAtPoint.addScaledVector( line3.dir , - scalarToBack )
      cameraLookAtPoint.addScaledVector( tmp_dir , tmp_dir_distance )

      lookAtPoint[0] = cameraLookAtPoint.x
      lookAtPoint[1] = cameraLookAtPoint.y
      lookAtPoint[2] = cameraLookAtPoint.z

      camera.lookAt( cameraLookAtPoint )

      line3.end.copy( cameraEyePoint )
      line3.start.copy( boundingSphere_prev.center )
      line3.dir.copy( line3.end ).sub( line3.start ).normalize()

    }

    detectCollisions(collisionables){

      if( line3.start.equals( line3.end ) )return

      collisionables.map( function( collisionable ){

        if( 
          boundingSphere.intersectsSphere(collisionable) || 
          boundingSphere_prev.intersectsSphere(collisionable)
        ){

          if( collisionable.collisionables ){

            collisionable.collisionables.map( function( plane ){

              if( 
                boundingSphere.intersectsSphere( plane.boundingSphere )
              ){
                
                let distance = Math.abs( plane.distanceToPoint( boundingSphere.center ) )
                
                capp.vue_container.distance1 = distance;

                if( distance < boundingSphere.radius ){

                  let projectPoint = plane.projectPoint( boundingSphere.center , tmpVector3 ).clone()

                  if( instance.quadContainsPoint( plane , projectPoint ) ){

                    let projectPoint_prev = plane.projectPoint( boundingSphere_prev.center , tmpVector3 ).clone()

                    let vunit1 = boundingSphere.center.clone().sub( projectPoint )
                    let vunit2 = boundingSphere_prev.center.clone().sub( projectPoint_prev )

                    let scalarToBack = 0

                    if( vunit1.dot( vunit2 ) >=0 ){

                      scalarToBack = ( boundingSphere.radius - vunit1.length() ) * boundingSphere.center.distanceTo( boundingSphere_prev.center )
                      scalarToBack /= ( vunit2.length() - vunit1.length() )

                    }else{

                      scalarToBack = ( boundingSphere.radius + vunit1.length() ) * boundingSphere.center.distanceTo( boundingSphere_prev.center )
                      scalarToBack /= ( vunit2.length() + vunit1.length() )

                    }
                    //scalarToBack += 0.1
                    let previous_bounding_sphere_center = boundingSphere.center.clone()
                    boundingSphere.center.addScaledVector( line3.dir ,  - scalarToBack )

                    let thrut_projectPoint = plane.projectPoint( boundingSphere.center , tmpVector3 ).clone()

                    instance.addjustPositionAfterCollision( previous_bounding_sphere_center , scalarToBack , projectPoint, projectPoint_prev , thrut_projectPoint )

                  }

                }

              }

            });

          }

        }

      });

    }
    detectCollisions1(collisionables){

      if( line3.start.equals( line3.end ) )return

      let idx,idy,idz,lightBullet_tmp,tmp; 
      let only_plane_collisions = [];

      collisionables.map( function( collisionable ){

        if( 
          boundingSphere.intersectsSphere(collisionable) || 
          boundingSphere_prev.intersectsSphere(collisionable)
        ){

          if( collisionable.collisionables ){

            collisionable.collisionables.map( function( plane ){

              if( 
                boundingSphere.intersectsSphere( plane.boundingSphere ) || 
                boundingSphere_prev.intersectsSphere( plane.boundingSphere )
              ){
                console.log("boundingSphere collision with plane")
                only_plane_collisions.push( plane )

              }

            });
          
          }

        }

      });

      if( only_plane_collisions.length>0 ){
        let plane = only_plane_collisions[0]
        let plane_intersects_line = plane.intersectLine(  line3 , tmpVector3 )
        let distance = plane.distanceToPoint( boundingSphere.center ) < boundingSphere.radius
        let distance_prev = plane.distanceToPoint( boundingSphere_prev.center ) < boundingSphere_prev.radius
        let distance_in_path = plane_intersects_line ? tmpVector3.distanceTo( line3.start ) <= line3.length() : false

        let distanceA , distanceB, distanceC, distanceD, diff

        if( !plane_intersects_line ){

          if( distance ){

            distanceA = plane.pointA.distanceTo( boundingSphere.center )
            distanceB = plane.pointB.distanceTo( boundingSphere.center )
            distanceC = plane.pointC.distanceTo( boundingSphere.center )
            distanceD = plane.pointD.distanceTo( boundingSphere.center )

            if(  
              distanceA <= boundingSphere.radius || 
              distanceB <= boundingSphere.radius || 
              distanceC <= boundingSphere.radius || 
              distanceD <= boundingSphere.radius
            ){

            }
          if( distanceA <= boundingSphere.radius ){

            diff = boundingSphere.radius - distanceA
            boundingSphere.center.addScaledVector( line3.dir, - diff )
            instance.setPositionFromThreeVector3( boundingSphere.center )

          }else if( distanceB <= boundingSphere.radius ){

            diff = boundingSphere.radius - distanceB
            boundingSphere.center.addScaledVector( line3.dir, - diff )
            instance.setPositionFromThreeVector3( boundingSphere.center )

          }else if( distanceC <= boundingSphere.radius ){
            
            diff = boundingSphere.radius - distanceC
            boundingSphere.center.addScaledVector( line3.dir, - diff )
            instance.setPositionFromThreeVector3( boundingSphere.center )

          }else if( distanceD <= boundingSphere.radius ){
            
            diff = boundingSphere.radius - distanceD
            boundingSphere.center.addScaledVector( line3.dir, - diff )
            instance.setPositionFromThreeVector3( boundingSphere.center )

          }else {

              plane.lineA.closestPointToPoint( boundingSphere.center , true , tmpVector3_3 )
              diff = tmpVector3_3.distanceTo( boundingSphere.center )
              if( diff < boundingSphere.radius ){
                tmpVector3_3.copy( boundingSphere.center ).addScaledVector( line3.dir, - ( boundingSphere.radius - diff ) )
                instance.setPositionFromThreeVector3( tmpVector3_3 )
              }
              plane.lineB.closestPointToPoint( boundingSphere.center , true , tmpVector3_3 )
              diff = tmpVector3_3.distanceTo( boundingSphere.center )
              if( diff < boundingSphere.radius ){
                tmpVector3_3.copy( boundingSphere.center ).addScaledVector( line3.dir, - ( boundingSphere.radius - diff ) )
                instance.setPositionFromThreeVector3( tmpVector3_3 )
              }
              plane.lineC.closestPointToPoint( boundingSphere.center , true , tmpVector3_3 )
              diff = tmpVector3_3.distanceTo( boundingSphere.center )
              if( diff < boundingSphere.radius ){
                tmpVector3_3.copy( boundingSphere.center ).addScaledVector( line3.dir, - ( boundingSphere.radius - diff ) )
                instance.setPositionFromThreeVector3( tmpVector3_3 )
              }
              plane.lineD.closestPointToPoint( boundingSphere.center , true , tmpVector3_3 )
              diff = tmpVector3_3.distanceTo( boundingSphere.center )
              if( diff < boundingSphere.radius ){
                tmpVector3_3.copy( boundingSphere.center ).addScaledVector( line3.dir, - ( boundingSphere.radius - diff ) )
                instance.setPositionFromThreeVector3( tmpVector3_3 )
              }
            }

          }

        }else{



        }

        if( 
            plane_intersects_line || 
            distance || 
            distance_prev || 
            distance_in_path 
        ){ 
          
          if( instance.quadContainsPoint (plane,tmpVector3 ) ){

            instance.moveToNearestAndAppyDisplacement(tmpVector3,plane)

          } 

        }

      }

    }

    moveToNearestAndAppyDisplacement(point_intersection,plane){
      return
      tmpVector3_2.copy( line3.end ).sub( line3.start ).normalize()

      bullet.position.copy( point_intersection ).addScaledVector( tmpVector3_2 , - boundingSphere.radius - 0.5 )
      
      bullet.position_prev.copy( bullet.position )

      bullet.line3.start.copy( bullet.position_prev );

      bullet.line3.end.copy( bullet.position );

      bullet.boundingSphere.center.copy( bullet.position );
      
      bullet.dir.reflect( plane.normal )

    }

  }

  return {
    getInstance(obj){
      if(!instance){
        instance = new CameraUtils(obj);
      }
      instance.init();
      return instance;
    }
  };
})();

export default func;