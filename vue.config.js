
const path = require("path");

module.exports = {
  devServer: {
    https: false,
  },
  productionSourceMap: false,
  outputDir: path.resolve(__dirname, "dist"),
  assetsDir: "./static",
  publicPath: (process.env.NODE_ENV === 'production'? '/games/shooter/' : '/'),
  /*publicPath: (process.env.NODE_ENV === 'production'? '/games/shooter_test/' : '/'),*/

  pluginOptions: {
    i18n: {
      locale: 'es',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  chainWebpack: config => {
  }
}

